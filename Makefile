APP_NAME=xtrade-base

build:
	docker build -t $(APP_NAME) .

deploy build:
	docker tag $(APP_NAME) registry.gitlab.com/camstuartdev/xtrade-container-image/$(APP_NAME)
	docker push registry.gitlab.com/camstuartdev/xtrade-container-image/$(APP_NAME)
