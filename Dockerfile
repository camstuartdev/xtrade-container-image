FROM python:3.10.4-buster

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

RUN apt update && apt -y upgrade
RUN apt -y --force-yes install build-essential libpq-dev

WORKDIR /tmp
RUN wget http://prdownloads.sourceforge.net/ta-lib/ta-lib-0.4.0-src.tar.gz
RUN tar -xzf ta-lib-0.4.0-src.tar.gz
WORKDIR /tmp/ta-lib
RUN ./configure --prefix=/usr && make && make install

WORKDIR /tmp

RUN rm -rf ta-*
